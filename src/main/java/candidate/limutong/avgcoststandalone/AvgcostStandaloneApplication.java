package candidate.limutong.avgcoststandalone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvgcostStandaloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvgcostStandaloneApplication.class, args);
    }


}
