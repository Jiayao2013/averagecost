package candidate.limutong.avgcoststandalone;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomePage {

    /**
     *
     *  http://localhost:8080/avg
     *
     **/
    @RequestMapping("/avg")
    public String showAvg(){

        return "avg V2.0";
    }


}
