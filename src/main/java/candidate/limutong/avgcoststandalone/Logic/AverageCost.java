package candidate.limutong.avgcoststandalone.Logic;

import candidate.limutong.avgcoststandalone.domain.Trade;
import scala.Tuple2;

import java.util.LinkedList;
import java.util.List;

public class AverageCost {

    public static List<Trade> run(List<Trade> inputs){

        Trade overNight = inputs.stream().filter(t->"OverNight".equalsIgnoreCase(t.getTrade())).findAny().orElse(null);
        if(null == overNight){
            System.out.println("[Data Quality Issue]: No Over Night Data");
            return inputs;
        }

        //out
        List<Trade> outputs = new LinkedList<>();
        //The current position
        Double Q = overNight.getPosition();
        //The current Total Cost
        Double TotalCost;
        //shares sold Today
        Double Qs=0D;
        //Sold Value Today
        Double SV=0D;
        //shares bot today
        Double Qb=0D;
        //BotValue today
        Double BV=0D;
        //BotAvgPx,SoldAvgPx
        Double BotAvgPx = 0D,SoldAvgPx =0D;
        //The OverNight Total Cost
        TotalCost = Math.abs(Q * overNight.getAverageCost());
        overNight.setTotalCost(TotalCost);
        overNight.setBotAvgPx(BotAvgPx);
        overNight.setSoldAvgPx(SoldAvgPx);
        outputs.add(overNight);

        for(int i = 1; i < inputs.size(); i++){

            System.out.println(i);
            Trade curT = inputs.get(i);
            Trade preT = outputs.get(i-1);
            Trade out = new Trade(curT);
            //BotAvgPx,SoldAvgPx,New Position
            if("Bot".equalsIgnoreCase(curT.getTrade())||"Buy".equalsIgnoreCase(curT.getTrade())){
                Q += curT.getFillQty();
                Qb += curT.getFillQty();
                BV += curT.getFillQty() * curT.getPrice();
                BotAvgPx = BV / Qb;
            }else{
                Q -= curT.getFillQty();
                Qs += curT.getFillQty();
                SV += curT.getFillQty() * curT.getPrice();
                SoldAvgPx = SV / Qs;
            }
            out.setSoldAvgPx(SoldAvgPx);
            out.setBotAvgPx(BotAvgPx);
            out.setPosition(Q);
            curT.setPosition(Q);
            //Avg Cost
            Tuple2<Double,Double> result= averageCostRules(curT,preT);
            Double avgCost = result._1;
            out.setAverageCost(avgCost);
            //Total Cost
            Double totalCost = result._2;
            out.setTotalCost(totalCost);

            outputs.add(out);
        }
        return outputs;
    }

    /**
     * 1)	If a trade, fill report, increases the position (long or short, in absolute terms), then average cost is updated based on the relative weight of the trade
     * 2)	If a trade decreases the position (long or short, in absolute terms), then average cost does not change
     * 3)	If a trade closes out a position, the average cost does not change.
     * 4)	If a trade changes the position from long to short or from short to long,  then portion of the fill that is not used to close out the previous position is used as the new average cost.
     **/
    public static Tuple2<Double,Double> averageCostRules(Trade curT, Trade preT) {

        Double avgCost = null;
        Double totCost = null;
        Double preP = preT.getPosition();
        boolean isPreLong = preP > 0?true:false;
        Double curP = curT.getPosition();
        boolean isCurLong = curP > 0?true:false;

        //3)
        if(Math.abs(curP) == 0D){
            totCost = 0D;
            avgCost = 0D;
        //1)
        }else if(isPreLong==isCurLong && Math.abs(curP)>Math.abs(preP)){
            totCost = (curP-preP) * curT.getPrice() + preP * preT.getAverageCost();
            avgCost = totCost / curP;
        //2)
        }else if(isPreLong==isCurLong && Math.abs(curP)<Math.abs(preP)){
            totCost = preT.getTotalCost();
            avgCost = preT.getAverageCost();
        //4)
        }else if(isPreLong!=isCurLong){
            totCost = curT.getPrice() * curP;
            avgCost = totCost / curP;
        }else{
            totCost = preT.getTotalCost();
            avgCost = preT.getAverageCost();
        }
        return new Tuple2<>(avgCost,totCost);
    }


}
