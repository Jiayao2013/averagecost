package candidate.limutong.avgcoststandalone.db;

import candidate.limutong.avgcoststandalone.domain.Trade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TradeRepository extends JpaRepository<Trade, Long> {

//    List<Trade> findByLastNameStartsWithIgnoreCase(String lastName);
}
