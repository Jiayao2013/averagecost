package candidate.limutong.avgcoststandalone.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Trade {

    @Id
    @GeneratedValue
    private Long id;
    private String trade;
    private Double fillQty;
    private Double price;
    private Double position;
    private Double totalCost;

    private Double averageCost;
    private Double botAvgPx;
    private Double soldAvgPx;

    protected Trade() {
    }

    public Trade(String trade, Double fillQty, Double price, Double position, Double totalCost, Double averageCost) {
        this.trade = trade;
        this.fillQty = fillQty;
        this.price = price;
        this.position = position;
        this.totalCost = totalCost;
        this.averageCost = averageCost;
    }

    public Trade(Trade trade) {
        this.id = trade.getId();
        this.trade = trade.getTrade();
        this.fillQty = trade.getFillQty();
        this.price = trade.getPrice();
        this.position = trade.getPosition();
        this.totalCost = trade.getTotalCost();
        this.averageCost = trade.getAverageCost();
    }

    public Double getAverageCost() {
        return averageCost;
    }

    public void setAverageCost(Double averageCost) {
        this.averageCost = averageCost;
    }

    public Double getBotAvgPx() {
        return botAvgPx;
    }

    public void setBotAvgPx(Double botAvgPx) {
        this.botAvgPx = botAvgPx;
    }

    public Double getSoldAvgPx() {
        return soldAvgPx;
    }

    public void setSoldAvgPx(Double soldAvgPx) {
        this.soldAvgPx = soldAvgPx;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFillQty() {
        return fillQty;
    }

    public void setFillQty(Double fillQty) {
        this.fillQty = fillQty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPosition() {
        return position;
    }

    public void setPosition(Double position) {
        this.position = position;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "id=" + id +
                ", trade='" + trade + '\'' +
                ", fillQty=" + fillQty +
                ", price=" + price +
                ", position=" + position +
                ", totalCost=" + totalCost +
                '}';
    }
}
