package candidate.limutong.avgcoststandalone.gui;

import candidate.limutong.avgcoststandalone.Logic.AverageCost;
import candidate.limutong.avgcoststandalone.db.TradeRepository;
import candidate.limutong.avgcoststandalone.domain.Trade;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route(value="grid")
public class MainView extends VerticalLayout implements HasUrlParameter<String>  {

    private final TradeRepository tradeRepository;

    private final TradeProcessor processor;

    //display grid
    final Grid<Trade> grid;

    private final Button execBtn;

    private String runMode = "ex1";

    public MainView(TradeRepository repo, TradeProcessor processor) {
        this.tradeRepository = repo;

        this.processor = processor;
        this.grid = new Grid<>(Trade.class);
        this.execBtn = new Button("Calculate Average Cost", VaadinIcon.ENTER.create());

        HorizontalLayout actions = new HorizontalLayout(execBtn);
        add( grid,actions, processor);

        grid.setHeight("310px");
        grid.setColumns("id", "trade", "fillQty", "price", "position");
        grid.getColumnByKey("id").setWidth("50px").setFlexGrow(0);

        execBtn.addClickListener(e -> processor.avgCost(AverageCost.run(tradeRepository.findAll())));

        processor.setChangeHandler(() -> {
            processor.setVisible(false);
        });

    }

    void listTrades(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(tradeRepository.findAll());
        }
    }

    public void loadDataExample1(TradeRepository repository) {
        repository.deleteAll();
        repository.save(new Trade("OverNight",null,null,200D,null,30D));
        repository.save(new Trade("Bot",200D,30.50D,null,null,null));
        repository.save(new Trade("Sell",300D,30.75D,null,null,null));
        repository.save(new Trade("Sell",400D,31.0D,null,null,null));
        repository.save(new Trade("Sell",400D,31.0D,null,null,null));
        repository.save(new Trade("Buy",700D,30D,null,null,null));
    }

    public void loadDataExample2(TradeRepository repository) {
        repository.deleteAll();
        repository.save(new Trade("OverNight",null,null,-500D,null,31.00D));
        repository.save(new Trade("Bot",400D,30D,null,null,null));
        repository.save(new Trade("Bot",600D,29D,null,null,null));
        repository.save(new Trade("Bot",200D,28D,null,null,null));
        repository.save(new Trade("Sold",400D,30D,null,null,null));
        repository.save(new Trade("Sold",400D,31D,null,null,null));
        repository.save(new Trade("Bot",500D,28D,null,null,null));
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        System.out.println("---------------- Param");
        System.out.println(s);
        this.runMode = s;
        if(runMode.contains("2")){
            loadDataExample2(tradeRepository);
        }else{
            loadDataExample1(tradeRepository);
        }
        listTrades(null);
    }
}
