package candidate.limutong.avgcoststandalone.gui;

import candidate.limutong.avgcoststandalone.db.TradeRepository;
import candidate.limutong.avgcoststandalone.domain.Trade;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@SpringComponent
@UIScope
public class TradeProcessor extends VerticalLayout implements KeyNotifier {

    private final TradeRepository repository;

    private Trade trade;

    //display grid
    final Grid<Trade> grid;

    @Autowired
    public TradeProcessor(TradeRepository repository) {
        this.repository = repository;
        this.grid = new Grid<>(Trade.class);

        HorizontalLayout actions = new HorizontalLayout();
        add(actions, grid);
        grid.setHeight("310px");
        grid.setColumns("id", "trade", "fillQty", "price", "position", "totalCost","averageCost","botAvgPx","soldAvgPx");

        setSpacing(true);
        setVisible(false);
    }

    public interface ChangeHandler {
        void onChange();
    }

    public final void avgCost(List<Trade> allTrades) {
        grid.setItems(allTrades);
        setVisible(true);
    }

    public void setChangeHandler(ChangeHandler h) {
    }
}
