package candidate.limutong.avgcoststandalone.Logic;

import candidate.limutong.avgcoststandalone.domain.Trade;
import org.junit.Test;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AverageCostTest {

    @Test
    public void run() {
        List<Trade> inputs = Arrays.asList(new Trade("Bot",null,null,-500D,null,31.00D),new Trade("Bot",400D,30D,null,null,null));
        List<Trade> outputs = AverageCost.run(inputs);
        assertNotNull(outputs);
    }

    @Test
    public void averageCostRules() {
        List<Trade> inputs = Arrays.asList(new Trade("Bot",400D,30D,-100D,15500D,31.0D),new Trade("Bot",600D,29D,500D,null,null));
        Tuple2<Double,Double> out = AverageCost.averageCostRules(inputs.get(0),inputs.get(1));
        assertEquals(29.0D,out._1,1D);
        assertEquals(-3000.0D,out._2,1D);
    }
}