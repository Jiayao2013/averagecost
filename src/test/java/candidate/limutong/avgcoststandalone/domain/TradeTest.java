package candidate.limutong.avgcoststandalone.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class TradeTest {

    @Test
    public void getAverageCost() {
        Trade t = new Trade();
        t.setAverageCost(199.99D);
        assertEquals(199.99D,t.getAverageCost(),0D);
    }

    @Test
    public void setFillQty() {
        Trade t = new Trade();
        t.setFillQty(28000D);
        assertEquals(28000D,t.getFillQty(),0D);
    }

    @Test
    public void toStringTest() {
        Trade t = new Trade();
        t.setFillQty(28000D);
        t.setAverageCost(199.99D);
        assertTrue(t.toString().length() > 1);
    }
}